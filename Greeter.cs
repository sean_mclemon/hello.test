﻿using System;

namespace Hello.Test
{
    public class Greeter
    {
        public string Execute(string name)
        {
            return $"Hello, {name}";
        }
    }
}
